<?php
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	include_once 'config/api_config.php';

	if(!isset($_REQUEST['q'])){
		http_response_code(404);
	    echo json_encode(array("message" => "No band name given." ));
	    exit(1);			
	}
	
	getBandId($_REQUEST['q']);

	function getBandId($bandName){
		$spotify= new spotifyService();
		$band= json_decode($spotify->findBandByName($bandName),true);
		if($band['artists']['total']==0){
			http_response_code(404);
		    echo json_encode(array("message" => "Band not found." ));	
		    exit(1);		
		}else{
			return getBandDiscography($band['artists']['items']['0']['id']);
		}
	}

	function getBandDiscography($bandId){
		$spotify= new spotifyService();
		$discography=json_decode($spotify->findBandDiscography($bandId), true);
		if(is_array($discography['items'])){
			$albums=array();
			foreach ($discography['items'] as $k => $v){
				array_push($albums, buildAlbumFormat($v));
			}
			echo json_encode($albums);
		}
		else {
			http_response_code(404);
		    echo json_encode(array("message" => "No albums to show." ));
		}		

	    exit(1);
	}

	function buildAlbumFormat($disco){
		$album=array();
			$album['names']=$disco['name'];
			$album['released']=$disco['release_date'];
			$album['tracks']=$disco['total_tracks'];
			$album['cover']['height']=$disco['images']['0']['height'];
			$album['cover']['width']=$disco['images']['0']['width'];
			$album['cover']['url']=$disco['images']['0']['url'];
		return $album;
	}

?>