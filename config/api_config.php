<?php

class spotifyService {

	public $config;
	public $baseUri;
	public $reqPrefs = array();

	public function __construct() {
		$this->config = parse_ini_file('config.ini', true);		
		$this->baseUri ='https://api.spotify.com/v1/';
		$this->reqPrefs['http']['method'] = 'GET';
		$this->reqPrefs['http']['header'] = 'Authorization: Bearer ' . $this->config['authToken'];
	}

	public function findBandByName($bandName){
		$resource = 'search?q='.urlencode($bandName).'&type=artist';
		return $this->do_GET($resource); 
	}


	public function findBandDiscography($bandId){
		$resource= 'artists/'.$bandId.'/albums';
		return $this->do_GET($resource);
	}

	function do_GET($resource) {
	  $req_headers = array($this->reqPrefs['http']['header']);
	  $info = array();
	  $url=$this->baseUri.$resource;
	  $response = $this->_do_GET($url, $info, $req_headers);
	  if ($info['http_code'] != 200) {
		echo "External resource exception:\n";
		echo $response . "\n";
		exit(1);
	  }
	  return $response;
	}

	function _do_GET($url, &$info, $req_headers = array()) {
	  $req_headers[] = 'Content-Type: application/json';
	  $curl = curl_init();
	  curl_setopt($curl, CURLOPT_URL, $url);
	  curl_setopt($curl, CURLOPT_SSLVERSION, 1);
	  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
	  curl_setopt($curl, CURLOPT_HEADER, true);
	  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($curl, CURLOPT_HTTPHEADER, $req_headers);
	  curl_setopt($curl, CURLOPT_HTTPGET, true);
	  $output = curl_exec($curl);
	  $info   = curl_getinfo($curl);
	  curl_close($curl);

	  $headers = substr($output, 0, $info['header_size']);
	  $headers = explode("\n", $headers);
	  $info['headers'] = $headers;
	  $body = substr($output, $info['header_size']);
	  return $body;
	}


}